/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 25/10/2021, lunes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DateFormatUtil {
    private static final Logger logger = LogManager.getLogger(DateFormatUtil.class);

    private static final String FORMAT_1 = "yyyy-MM-dd HH:mm:ss.S";
    private static final String FORMAT_2 = "dd-MM-yyyy";
    private static final String FORMAT_3 = "MM/dd/yyyy HH:mm";
    private static final String MESSAGE_FORMAT_1 = "Invalid date or invalid date format, enter with format: " +FORMAT_1;
    private static final String MESSAGE_FORMAT_2 = "Invalid date or invalid date format, enter with format: " +FORMAT_2;
    private static final String MESSAGE_FORMAT_3 = "Invalid date or invalid date format, enter with format: " +FORMAT_3;

    public String convTimesString(Timestamp date) {
        String respose = "";
        try {
            respose = new SimpleDateFormat(FORMAT_3).format(date);
        } catch (Exception e) {
            logger.info("Error to convert[Timestamp to String] value");
            e.printStackTrace();
        }
        return respose;
    }

    public Date returnDate(String date) {
        Date respose = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_1);
            LocalDate dateTime = LocalDate.parse(date, formatter);
            respose = Date.valueOf(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respose;

    }

    public LocalDate returnLocalDateF1(String date) {
        LocalDate respose = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_1);
            LocalDate dateTime = LocalDate.parse(date, formatter);
            respose = dateTime;
        } catch (Exception e) {
            throw new IllegalArgumentException(MESSAGE_FORMAT_1);
        }
        return respose;

    }

    public LocalDate returnLocalDateF2(String date) {
        LocalDate respose = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_2);
            LocalDate dateTime = LocalDate.parse(date, formatter);
            respose = dateTime;
        } catch (Exception e) {
            throw new IllegalArgumentException(MESSAGE_FORMAT_2);
        }
        return respose;

    }

    public int obtainDays(LocalDate from, LocalDate to){
        int numberDays= 0;
        Period period = Period.between(from, to);
        numberDays = period.getDays();
        return numberDays;
    }
    public LocalDate addDays(LocalDate date, long nDays){
        LocalDate result = date;
        if(nDays < 0){
            long absDays= Math.abs(nDays);
            result = date.minusDays(absDays);
        }else{
            result = date.plusDays(nDays);
        }
        return result;
    }

}
