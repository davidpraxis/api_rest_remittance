/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.utils.constants;

public class AnulaConstants {

    public static final String SUCCESS_MESSAGE="Cancel Policy success";

    public static final String NOT_SUCCESS_MESSAGE="Cancel Policy not success";
}
