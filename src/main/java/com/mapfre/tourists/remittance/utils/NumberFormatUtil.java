/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 24/09/2021, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;

public class NumberFormatUtil {
    private static final Logger logger = LogManager.getLogger(NumberFormatUtil.class);

    public Integer convertToInteger(String value){
        Integer number=0;
        try{
             number = Integer.parseInt(value);
        }
        catch (NumberFormatException ex){
            logger.info("Error to convert[String to Int] value");
        }
        return number;
    }

    public Long convertToLong(String value) {
        long res = 0;
        try {
            //Conversion using valueOf(String) method
            res = Long.valueOf(value);
        } catch (Exception e) {
            logger.info("Error to convert[String to Long] value");
        }
        return res;
    }

    public  BigInteger convertToBigInteger(String number){
        BigInteger value= BigInteger.ZERO;
        try {
            value = new BigInteger(number);
        } catch (Exception e) {
            logger.info("Error to convert[String to BigInteger] value");
        }
        return value;
    }

    public Double convertToDouble(String value) {
        Double res = 0.0;
        try {
            //Conversion using valueOf(String) method
            res = Double.valueOf(value);
        } catch (Exception e) {
            logger.info("Error to convert[String to Double] value");
        }
        return res;
    }


}
