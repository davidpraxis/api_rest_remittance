/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 26/01/2022, miércoles
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.utils.constants;

public class StatusConstants {

    public static final String OPERATION_SUCCESS = "OPERACION EXITOSA: ";
    public static final String OPERATION_NOT_SUCCESS = "OPERACION NO EXITOSA: ";

    public static class Errors {
        public static final String ERROR_DATA="Error en los datos";
        public static final String ERROR_CAMPO_EMPTY = "No existe el campo: ";
        public static final String ERROR_CAMPO_INVALID = "Es invalido el campo: ";
    }

    public static class DatosMinimos {
        public static final String CODE_RAMO = "Código de ramo";
        public static final String CMP_VAL = "Código de la compañia";
        public static final String USR_VAL = "Código de usuario";
        public static final String LGN_VAL = "Idioma";
    }
}
