/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.utils.constants;

public class CondicionesConstants {

    public static final String OPERATION_SUCCESS = "OPERACION EXITOSA: ";
    public static final String OPERATION_NOT_SUCCESS = "OPERACION NO EXITOSA: ";


    public static class CondicionesTest{
        public static final String[] AGENTES_VALIDOS = {"99998", "99999", "5988", "5978"};
        public static final Double L_NUM_COMISION = 23.0;
        public static final Double L_NUM_SOBRECOMISION = 13.0;
        public static final Double L_DER_POL_AGT_SAL = 0.0;
        public static final Double L_DER_POL_AGT = 12.0;
    }


    public static class Errors{
        public static final String ERROR_CAMPO_EMPTY = "No existe el campo: ";
        public static final String ERROR_CAMPO_INVALID = "Es invalido el campo: ";

        public static final Integer CODE_ERROR= -20001;
        public static final String L_ERROR = "THE POLICY FEE ENTERED IS GREATER THAN THE POLICY FEE ASIGNED. PLEASE CHECK THE AGENT¨S CONFIGURATION.";
        public static final String L_ERROR_RANGO = "THE POLICY FEE IS OUT OF RANGE.";
        public static final String L_ERROR_MON_AB = "PLEASE CHECK THE AGENT¨S CONFIGURATION";
        public static final String L_EXISTE = "THE AGENT DOES NOT EXIST.";
    }

    public static class DatosMinimos {
        public static final String CMP_VAL = "Código de la compañia";
        public static final String USR_VAL = "Código de usuario";
        public static final String LGN_VAL = "Idioma";
        public static final String P_AGENTE = "Código del agente";
        public static final String P_CONTRATO = "Número de contrato";
        public static final String P_COD_RAMO = "Código de ramo";
        public static final String P_POLIZAS = "Número de cantidad de pólizas";
        public static final String P_CADENA_POLIZAS = "Números de pólizas";
        public static final String P_PRIMA_NETA = "Prima neta";
        public static final String P_DER_POLIZA = "Derechos de póliza";
        public static final String P_PRIMA_TOTAL = "Prima total";

    }
}
