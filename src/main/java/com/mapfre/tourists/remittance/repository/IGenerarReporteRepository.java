package com.mapfre.tourists.remittance.repository;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.mapfre.tourists.remittance.entity.GenerarReporteRemittance;


public interface IGenerarReporteRepository extends JpaRepository<GenerarReporteRemittance, Long> {
	
	@Procedure(procedureName = "GenerarReporteRemittance.generarReporteRemittance")
	Map<String, Object> generarReporteRemittance(
			@Param("l_identificador") String l_identificador,
			@Param("l_tip_consulta") Integer l_tip_consulta,
			@Param("l_apply") Integer l_apply,
			@Param("l_contrato") Integer l_contrato,
			@Param("l_agente") String l_agente,
			@Param("l_aviso_pago") Integer l_aviso_pago,
			@Param("l_poliza") String l_poliza,
			@Param("l_from") String l_from,
			@Param("l_to") String l_to
			);

}
