package com.mapfre.tourists.remittance.repository;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.mapfre.tourists.remittance.entity.GeneratePayment;

public interface IPaymentGenerateRepository extends JpaRepository<GeneratePayment, Long> {

	/*
	 * Falta validación de tipos de datos y mapeo de campos
	 */
	@Procedure(procedureName = "PaymentGenerate.paymentGenerate")
	Map<String, Object> paymentGenerate(
			@Param("lp_cod_ramo") Integer lp_cod_ramo, 
			@Param("l_agente") String l_agente,
			@Param("l_p_neta") Integer l_p_neta, 
			@Param("l_impuestos") Integer l_impuestos,
			@Param("l_comision") Integer l_comision, 
			@Param("l_dp_agt") Integer l_dp_agt,
			@Param("usuario") String usuario, 
			@Param("p_banco") String p_banco,
			@Param("l_suplemento") Integer l_suplemento, 
			@Param("l_iva_trasladado") Integer l_iva_trasladado,
			@Param("l_iva_retenido") Integer l_iva_retenido, 
			@Param("l_agt_tur") String l_agt_tur,
			@Param("l_der_pol") Integer l_der_pol, 
			@Param("l_p_total") Integer l_p_total,
			@Param("l_sb_comision") Integer l_sb_comision, 
			@Param("l_imp_agt") Integer l_imp_agt,
			@Param("l_Poliza") String l_Poliza, 
			@Param("p_orden") String p_orden,
			@Param("l_isr_retenido") Integer l_isr_retenido);
}
