package com.mapfre.tourists.remittance.repository;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.mapfre.tourists.remittance.entity.ConsultaRemittance;

public interface IConsultaRemittanceRepository extends JpaRepository<ConsultaRemittance, Long> {
        
    @Procedure(procedureName = "ConsultaRemittance.consultaRemittance")
    Map<String, Object> consultaRemittance(
								            @Param( "lp_cod_ramo") Integer lp_cod_ramo,
								            @Param( "tipConsulta") Integer tipConsulta,
								            @Param( "pagina") Integer pagina,
								            @Param( "registros") Integer registros,
								            @Param( "ordenar") String ordenar,
								            @Param( "tipOrden") String tipOrden,
								            @Param( "l_agente") String l_agente,
								            @Param( "l_num_contrato") String l_num_contrato,
								            @Param( "l_fec_from") String l_fec_from,
								            @Param( "l_fec_to") String l_fec_to,
								            @Param( "l_nom_agt") String l_nom_agt,
								            @Param( "l_num_aviso_pago") String l_num_aviso_pago,
								            @Param( "l_ape_pat") String l_ape_pat  );

}
