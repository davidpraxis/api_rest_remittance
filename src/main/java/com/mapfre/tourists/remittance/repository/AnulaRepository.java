/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.repository;

import com.mapfre.tourists.remittance.entity.Anula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AnulaRepository extends JpaRepository<Anula, Long> {

    @Procedure(procedureName = "Anula.cancelPolicy")
    String cancelPolicy(@Param("l_cod_ramo") Integer branchCode,
                       @Param("l_aviso_paso") Integer userCode
                       );

}
