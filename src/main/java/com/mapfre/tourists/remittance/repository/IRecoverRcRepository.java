package com.mapfre.tourists.remittance.repository;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.mapfre.tourists.remittance.entity.RecoverRc;

public interface IRecoverRcRepository extends JpaRepository<RecoverRc, Long> {

	@Procedure(procedureName = "RecoverRc.recoverRc")
	Map<String, Object> recoverRc(
			@Param("l_remittance") Integer lRemittance, 
			@Param("lp_cod_ramo") String lpCodRamo
			);

}
