package com.mapfre.tourists.remittance.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mapfre.tourists.remittance.model.request.GenerarReporteRequest;
import com.mapfre.tourists.remittance.model.response.GenerarReporteResponse;
import com.mapfre.tourists.remittance.service.GenerarReporteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api( value = "Remittance" )
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/tourists/remittance")
public class GenerarReporteController {
	
	private static final Logger logger = LogManager.getLogger(GenerarReporteController.class);
	
	@Autowired
	private GenerarReporteService generarReporteService;

	@ApiOperation( value = "Mock Operación que recupera consulta de remittance")
	@ApiResponses( value = {
			@ApiResponse( code = 200, message = "Operación exitosa"),
			@ApiResponse( code = 400, message = "Error en la operación"),
			@ApiResponse( code = 500, message = "Error Interno")
	})
	@RequestMapping(path = "/mock/generate/report", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenerarReporteResponse> mockGenerarReporteRemittance(@RequestBody GenerarReporteRequest request) {
		
		GenerarReporteResponse response = new GenerarReporteResponse();
		
		try {
			logger.debug("genera mock reporte...");
			response = generarReporteService.generarReporteMock(request);
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<GenerarReporteResponse>(response, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<GenerarReporteResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation( value = "Operación que recupera consulta de remittance")
	@ApiResponses( value = {
			@ApiResponse( code = 200, message = "Operación exitosa"),
			@ApiResponse( code = 400, message = "Error en la operación"),
			@ApiResponse( code = 500, message = "Error Interno")
	})
	@RequestMapping(path = "/generate/report", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenerarReporteResponse> generarReporteRemittance(@RequestBody GenerarReporteRequest request) {
		
		GenerarReporteResponse response = new GenerarReporteResponse();
		
		try {
			logger.debug("genera reporte...");
			response = generarReporteService.generarReporte(request);
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<GenerarReporteResponse>(response, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<GenerarReporteResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
