package com.mapfre.tourists.remittance.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mapfre.tourists.remittance.model.request.ConsultaRemittanceRequest;
import com.mapfre.tourists.remittance.model.response.ConsultaRemittanceResponse;
import com.mapfre.tourists.remittance.service.ConsultaRemittanceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api( value = "Remittance" )
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/tourists/remittance")
public class ConsultaRemittanceController {
	
	private static final Logger logger = LogManager.getLogger(ConsultaRemittanceController.class);
	
	@Autowired
	ConsultaRemittanceService consultaService;
	
	@ApiOperation( value = "Mock para consultar  remittance")
	@ApiResponses( value = {
			@ApiResponse( code = 200, message = "Operación exitosa"),
			@ApiResponse( code = 400, message = "Error en la operación"),
			@ApiResponse( code = 500, message = "Error Interno")
	})
	@RequestMapping(path = "/mock/consulta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaRemittanceResponse> mockConsultaRemittance(@RequestBody ConsultaRemittanceRequest request)
	{
		ConsultaRemittanceResponse result = new ConsultaRemittanceResponse();
		try {
				logger.debug("genera mock consulta ...");
				result = consultaService.consultaRemittanceMock(request);
				logger.debug("Ejecución exitosa...");
				return new ResponseEntity<ConsultaRemittanceResponse>(result, HttpStatus.OK);

		}catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e );
			e.printStackTrace();
			return new ResponseEntity<ConsultaRemittanceResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation( value = "Servicio para consultar  remittance")
	@ApiResponses( value = {
			@ApiResponse( code = 200, message = "Operación exitosa"),
			@ApiResponse( code = 400, message = "Error en la operación"),
			@ApiResponse( code = 500, message = "Error Interno")
	})
	@RequestMapping(path = "/consulta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaRemittanceResponse> consultaRemittance(@RequestBody ConsultaRemittanceRequest request)
	{
		ConsultaRemittanceResponse result = new ConsultaRemittanceResponse();
		try {
				logger.debug("genera mock consulta ...");
				result = consultaService.consultaRemittance(request);
				logger.debug("Ejecución exitosa...");
				return new ResponseEntity<ConsultaRemittanceResponse>(result, HttpStatus.OK);

		}catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e );
			e.printStackTrace();
			return new ResponseEntity<ConsultaRemittanceResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
