/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.controller;

import com.mapfre.tourists.remittance.model.request.RecuperaCondicionesRequest;
import com.mapfre.tourists.remittance.model.response.AnulaResponse;
import com.mapfre.tourists.remittance.model.response.RecuperaCondicionesResponse;
import com.mapfre.tourists.remittance.service.RecuperaCondicionesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value="Remittance")
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/tourits/remittance")
@RestController
public class RecuperaCondicionesController {

    @Autowired
    RecuperaCondicionesService service;

    @ApiOperation(value = "Mock para recuperar condiciones")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación exitosa"),
            @ApiResponse(code = 400, message = "Error en la operación"),
            @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping("/mock/recover/conditions")
    public ResponseEntity<RecuperaCondicionesResponse> recuperaCondicionesMock(@RequestBody RecuperaCondicionesRequest request){

        RecuperaCondicionesResponse response;
        try {
            response = service.recuperaCondicionesMock(request);
            if(response != null) {
                return new ResponseEntity<RecuperaCondicionesResponse>(response, HttpStatus.OK);
            }else{
                return new ResponseEntity<RecuperaCondicionesResponse>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<RecuperaCondicionesResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
