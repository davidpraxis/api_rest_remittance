package com.mapfre.tourists.remittance.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mapfre.tourists.remittance.model.request.RecoverRcRequest;
import com.mapfre.tourists.remittance.model.response.RecoverRcResponse;
import com.mapfre.tourists.remittance.service.RecoverRcService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Remittance")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/tourist/remittance")
public class RecoverRcController {

	private static final Logger logger = LogManager.getLogger(RecoverRcController.class);

	@Autowired
	RecoverRcService recoverService;

	@ApiOperation(value = "Mock recuperar las polizas asociadas al remittance")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Error en la operación"),
			@ApiResponse(code = 500, message = "Error Interno") })
	@RequestMapping(path = "/mock/recover/rc", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RecoverRcResponse> mockRecoverRc(@RequestParam Integer l_remittance, @RequestParam String lp_cod_ramo) {
		
		RecoverRcResponse result = new RecoverRcResponse();
		try {
			logger.debug("genera mock consulta ...");
			result = recoverService.recoverRcMock(new RecoverRcRequest(l_remittance, lp_cod_ramo));
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<RecoverRcResponse>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e);
			e.printStackTrace();
			return new ResponseEntity<RecoverRcResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Recuperación de las polizas asociadas al remittance")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Error en la operación"),
			@ApiResponse(code = 500, message = "Error Interno") })
	@RequestMapping(path = "/recover/rc", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RecoverRcResponse> recoverRc(@RequestParam Integer l_remittance, @RequestParam String lp_cod_ramo) {
		
		RecoverRcResponse result = new RecoverRcResponse();
		try {
			logger.debug("genera consulta ...");
			result = recoverService.recoverRc(new RecoverRcRequest(l_remittance, lp_cod_ramo));
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<RecoverRcResponse>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e);
			e.printStackTrace();
			return new ResponseEntity<RecoverRcResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
