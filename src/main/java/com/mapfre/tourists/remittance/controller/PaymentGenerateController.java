package com.mapfre.tourists.remittance.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mapfre.tourists.remittance.model.request.PaymentGenerateRequet;
import com.mapfre.tourists.remittance.model.response.PaymentGenerateResponse;
import com.mapfre.tourists.remittance.service.PaymentGenerateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Remittance")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/tourist/remittance")

public class PaymentGenerateController {
	private static final Logger logger = LogManager.getLogger(PaymentGenerateController.class);

	@Autowired
	PaymentGenerateService paymentService;

	@ApiOperation(value = "Mock para generar aviso de pago por ramo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Error en la operación"),
			@ApiResponse(code = 500, message = "Error Interno") })
	@RequestMapping(path = "/mock/lineOfBusiness/payment/generate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentGenerateResponse> mockPaymentGenerate(@RequestBody PaymentGenerateRequet request) {

		PaymentGenerateResponse result = new PaymentGenerateResponse();
		try {
			logger.debug("genera mock de aviso de pago por ramo ...");
			result = paymentService.generatePaymentMock(request);
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<PaymentGenerateResponse>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e);
			e.printStackTrace();
			return new ResponseEntity<PaymentGenerateResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Operación para generar aviso de pago por ramo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Error en la operación"),
			@ApiResponse(code = 500, message = "Error Interno") })
	@RequestMapping(path = "/lineOfBusiness/payment/generate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentGenerateResponse> paymentGenerate(@RequestBody PaymentGenerateRequet request) {

		PaymentGenerateResponse result = new PaymentGenerateResponse();
		try {
			logger.debug("genera aviso de pago por ramo ...");
			result = paymentService.generatePayment(request);
			logger.debug("Ejecución exitosa...");
			return new ResponseEntity<PaymentGenerateResponse>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: " + e.getMessage(), e);
			e.printStackTrace();
			return new ResponseEntity<PaymentGenerateResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
