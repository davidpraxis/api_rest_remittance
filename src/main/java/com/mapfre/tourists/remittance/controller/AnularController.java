/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 17/01/2022, lunes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.controller;

import com.mapfre.tourists.remittance.model.request.AnulaRequest;
import com.mapfre.tourists.remittance.model.response.AnulaResponse;
import com.mapfre.tourists.remittance.service.AnulaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value="Remittance")
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/tourits/remittance")
@RestController
public class AnularController {

    @Autowired
    AnulaService anulaService;

    @ApiOperation(value = "Mock para anular  remittance")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación exitosa"),
            @ApiResponse(code = 400, message = "Error en la operación"),
            @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping("/mock/anular")
    public ResponseEntity<AnulaResponse> anulaPolizasMock(@RequestBody AnulaRequest request){
        AnulaResponse response ;
        try {
            response = anulaService.cancelPolicyMock(request);
            if(response != null) {
                return new ResponseEntity<AnulaResponse>(response, HttpStatus.OK);
            }else{
                return new ResponseEntity<AnulaResponse>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity<AnulaResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "PROCEDIMIENTO PARA DES-ASOCIAR EL REMMITANCE DE LAS POLIZAS\n" +
            " SIEMPRE Y CUANDO LAS POLIZAS TENGAN ESTATUS 1 Y 2.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación exitosa"),
            @ApiResponse(code = 400, message = "Error en la operación"),
            @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping("/anular")
    public ResponseEntity<AnulaResponse> anulaPolizas(@RequestBody AnulaRequest request){
        AnulaResponse response ;
        try {
            response = anulaService.cancelPolicy(request);
            if(response != null) {
                return new ResponseEntity<AnulaResponse>(response, HttpStatus.OK);
            }else{
                return new ResponseEntity<AnulaResponse>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity<AnulaResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
