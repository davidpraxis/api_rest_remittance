/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 25/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.controller;

import com.mapfre.tourists.remittance.model.request.StatusRemmitanceRequest;
import com.mapfre.tourists.remittance.model.response.StatusRemmitanceResponse;
import com.mapfre.tourists.remittance.service.StatusRemmitanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value="Remittance")
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/tourits/remittance")
@RestController
public class StatusRemmitanceController {

    @Autowired
    StatusRemmitanceService statusRemmitanceService;

    @ApiOperation(value = "Mock para obtener status remmitance")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación exitosa"),
            @ApiResponse(code = 400, message = "Error en la operación"),
            @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping("/mock/remittance/status")
    public ResponseEntity<StatusRemmitanceResponse> regresaStatusRemmitance(@RequestBody StatusRemmitanceRequest request){

        StatusRemmitanceResponse response;
        try {
            response = statusRemmitanceService.getStatusRemmitanceMock(request);
            if(response != null) {
                return new ResponseEntity<StatusRemmitanceResponse>(response, HttpStatus.OK);
            }else{
                return new ResponseEntity<StatusRemmitanceResponse>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<StatusRemmitanceResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
