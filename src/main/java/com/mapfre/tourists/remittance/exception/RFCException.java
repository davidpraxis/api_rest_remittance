package com.mapfre.tourists.remittance.exception;

public class RFCException extends RuntimeException {

	public RFCException(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
	}
	
	public RFCException(final String mensaje) {
		super(mensaje);
	}
	
	public RFCException() {
		super();
	}

}
