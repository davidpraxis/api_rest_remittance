package com.mapfre.tourists.remittance.exception;

public class UserNotFound extends RuntimeException {
	
	public UserNotFound(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
	}

	public UserNotFound(final String mensaje) {
		super(mensaje);
	}

	public UserNotFound() {
		super();
	}

}
