package com.mapfre.tourists.remittance.exception;

public class NoContentException extends RuntimeException {
	
	public NoContentException(final String mensaje) {
		super(mensaje);
	}
	
	public NoContentException() {
		super();
	}

}
