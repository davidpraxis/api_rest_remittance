package com.mapfre.tourists.remittance.exception;

public class DataConflictException extends RuntimeException {

	public DataConflictException(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
	}

	public DataConflictException(final String mensaje) {
		super(mensaje);
	}

	public DataConflictException() {
		super();
	}

}
