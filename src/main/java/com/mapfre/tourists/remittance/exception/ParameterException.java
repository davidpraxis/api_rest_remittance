package com.mapfre.tourists.remittance.exception;

public class ParameterException extends RuntimeException {

    public ParameterException(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
	}
	
	public ParameterException(final String mensaje) {
		super(mensaje);
	}
	
	public ParameterException() {
		super();
	}
    
}
