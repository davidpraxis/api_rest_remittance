package com.mapfre.tourists.remittance.exception;

public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1347831993154837967L;
	
	public ResourceNotFoundException(final String mensaje) {
		super(mensaje);
	}
	
	public ResourceNotFoundException() {
		super();
	}

}
