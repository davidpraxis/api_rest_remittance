package com.mapfre.tourists.remittance.model.request;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Servicio que genera reporte remittance.")
public class GenerarReporteRequest implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4101235079566175565L;

	/*'Número de poliza'*/
    @ApiModelProperty(value = "l_identificador", required = true, example = "12245")
    private String l_identificador;
    
    /*'Tipo de consulta'*/
    @ApiModelProperty(value = "l_tip_consulta", required = true, example = "2")
    private Integer l_tip_consulta;
    
    /*'Fecha de aplicación'*/
    @ApiModelProperty(value = "l_apply", required = true, example = "05/06/2021")
    private String l_apply;
    
    /*'Número de contrato'*/
    @ApiModelProperty(value = "l_contrato", required = true, example = "12012")
    private Integer l_contrato;
    
    /*'Código de agente'*/
    @ApiModelProperty(value = "l_agente", required = true, example = "A1254")
    private String l_agente;
    
    /*'Número de aviso de pago'*/
    @ApiModelProperty(value = "l_aviso_pago", required = true, example = "N4")
    private String l_aviso_pago;
    
    /*'Número de póliza'*/
    @ApiModelProperty(value = "l_poliza", required = true, example = "5154425")
    private String l_poliza;
    
    /*'Fecha de inicio'*/
    @ApiModelProperty(value = "l_from", required = true, example = "05/06/2021")
    private String l_from;
    
    /*'Fecha fin'*/
    @ApiModelProperty(value = "l_to", required = true, example = "05/06/2021")
    private String l_to;

	public String getL_identificador() {
		return l_identificador;
	}

	public void setL_identificador(String l_identificador) {
		this.l_identificador = l_identificador;
	}

	public Integer getL_tip_consulta() {
		return l_tip_consulta;
	}

	public void setL_tip_consulta(Integer l_tip_consulta) {
		this.l_tip_consulta = l_tip_consulta;
	}

	public String getL_apply() {
		return l_apply;
	}

	public void setL_apply(String l_apply) {
		this.l_apply = l_apply;
	}

	public Integer getL_contrato() {
		return l_contrato;
	}

	public void setL_contrato(Integer l_contrato) {
		this.l_contrato = l_contrato;
	}

	public String getL_agente() {
		return l_agente;
	}

	public void setL_agente(String l_agente) {
		this.l_agente = l_agente;
	}

	public String getL_aviso_pago() {
		return l_aviso_pago;
	}

	public void setL_aviso_pago(String l_aviso_pago) {
		this.l_aviso_pago = l_aviso_pago;
	}

	public String getL_poliza() {
		return l_poliza;
	}

	public void setL_poliza(String l_poliza) {
		this.l_poliza = l_poliza;
	}

	public String getL_from() {
		return l_from;
	}

	public void setL_from(String l_from) {
		this.l_from = l_from;
	}

	public String getL_to() {
		return l_to;
	}

	public void setL_to(String l_to) {
		this.l_to = l_to;
	}
}
