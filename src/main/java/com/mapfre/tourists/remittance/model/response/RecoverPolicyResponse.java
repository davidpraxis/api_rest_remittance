package com.mapfre.tourists.remittance.model.response;

import java.io.Serializable;

public class RecoverPolicyResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String policy;
	private String fecha;
	private String premium;
	private String policy_fee;
	private String tax;
	private String total;
	private Integer r6;
	private Integer r7;

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getPolicy_fee() {
		return policy_fee;
	}

	public void setPolicy_fee(String policy_fee) {
		this.policy_fee = policy_fee;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public Integer getR6() {
		return r6;
	}

	public void setR6(Integer r6) {
		this.r6 = r6;
	}

	public Integer getR7() {
		return r7;
	}

	public void setR7(Integer r7) {
		this.r7 = r7;
	}

}
