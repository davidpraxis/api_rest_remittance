package com.mapfre.tourists.remittance.model.response;

import java.io.Serializable;
import java.util.List;

import com.mapfre.tourists.remittance.model.RemittanceData;

public class ConsultaRemittanceResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2683253582018164421L;
	
	
	
	private List<RemittanceData> consulta;



	public List<RemittanceData> getConsulta() {
		return consulta;
	}

	public void setConsulta(List<RemittanceData> consulta) {
		this.consulta = consulta;
	}
	
	
}
