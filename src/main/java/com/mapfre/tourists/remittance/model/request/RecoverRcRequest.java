package com.mapfre.tourists.remittance.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Cadena con los datos mínimos para la consulta de polizas asociadas al remmitance")
public class RecoverRcRequest {

	@ApiModelProperty(value = "l_remittance", required = true, example = "500")
	public Integer lRemittance;

	@ApiModelProperty(value = "lp_cod_ramo", required = true, example = "405")
	private String lpCodRamo;

	
	public RecoverRcRequest(Integer lRemittance, String lpCodRamo) {
		super();
		this.lRemittance = lRemittance;
		this.lpCodRamo = lpCodRamo;
	}

	public Integer getlRemittance() {
		return lRemittance;
	}

	public void setlRemittance(Integer lRemittance) {
		this.lRemittance = lRemittance;
	}

	public String getLpCodRamo() {
		return lpCodRamo;
	}

	public void setLpCodRamo(String lpCodRamo) {
		this.lpCodRamo = lpCodRamo;
	}

}
