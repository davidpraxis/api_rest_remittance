package com.mapfre.tourists.remittance.model.response;

public class PaymentGenerateResponse {

	/**
	 * Respuesta sobre la operación de generacion de aviso de pago por ramo
	 */

	/* Número de aviso de pago */
	private String l_resultado;

	public String getL_resultado() {
		return l_resultado;
	}

	public void setL_resultado(String l_resultado) {
		this.l_resultado = l_resultado;
	}

}
