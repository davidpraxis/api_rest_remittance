/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 25/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Cadena con los datos mínimos para RECUPERAR STATUS REMMITANCE")
public class StatusRemmitanceRequest {

    /**
     * description: 'Código de ramo '
     */
    @ApiModelProperty(value = "ramo", required = true, example = "500")
    private Integer ramo;//I_ramo
    /**
     * description: Código de la compañia
     */
    @ApiModelProperty(value = "cmpVal", required = true, example = "1")
    private String cmpVal;
    /**
     * description: Código de usuario
     */
    @ApiModelProperty(value = "usrVal", required = true, example = "albertoj")
    private String usrVal;
    /**
     * description: Idioma (ES)
     */
    @ApiModelProperty(value = "lngVal", required = true, example = "ES")
    private String lngVal;

    public Integer getRamo() {
        return ramo;
    }

    public void setRamo(Integer ramo) {
        this.ramo = ramo;
    }

    public String getCmpVal() {
        return cmpVal;
    }

    public void setCmpVal(String cmpVal) {
        this.cmpVal = cmpVal;
    }

    public String getUsrVal() {
        return usrVal;
    }

    public void setUsrVal(String usrVal) {
        this.usrVal = usrVal;
    }

    public String getLngVal() {
        return lngVal;
    }

    public void setLngVal(String lngVal) {
        this.lngVal = lngVal;
    }
}
