/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 17/01/2022, lunes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Cadena con los datos mínimos para DES-ASOCIAR EL REMMITANCE DE LAS POLIZA")
public class AnulaRequest {

    /*'Código de ramo '*/
    @ApiModelProperty(value = "cod_ramo", required = true, example = "500")
    public Integer l_cod_ramo;

    /*Número de aviso de pago(remittance)*/
    @ApiModelProperty(value = "aviso_paso", required = true, example = "5")
    public Integer l_aviso_paso;


    public Integer getL_cod_ramo() {
        return l_cod_ramo;
    }

    public void setL_cod_ramo(Integer l_cod_ramo) {
        this.l_cod_ramo = l_cod_ramo;
    }

    public Integer getL_aviso_paso() {
        return l_aviso_paso;
    }

    public void setL_aviso_paso(Integer l_aviso_paso) {
        this.l_aviso_paso = l_aviso_paso;
    }
}
