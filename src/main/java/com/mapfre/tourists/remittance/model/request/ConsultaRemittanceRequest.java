package com.mapfre.tourists.remittance.model.request;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class ConsultaRemittanceRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2683253582018164421L;
	
	@ApiModelProperty(value = "lp_cod_ramo", required = true, example = "405")
	private Integer lp_cod_ramo;
	@ApiModelProperty(value = "registros", required = true, example = "2")
	private Integer registros;
	@ApiModelProperty(value = "tipOrden", required = true, example = "1")
	private String tipOrden;
	@ApiModelProperty(value = "l_num_contrato", required = true, example = "456456")
	private String l_num_contrato;
	@ApiModelProperty(value = "l_fec_to", required = true, example = "05/06/2021")
	private String l_fec_to;
	@ApiModelProperty(value = "l_num_aviso_pago", required = true, example = "34")
	private String l_num_aviso_pago;
	@ApiModelProperty(value = "tipConsulta", required = true, example = "3")
	private Integer tipConsulta;
	@ApiModelProperty(value = "ordenar", required = true, example = "2")
	private Integer ordenar;
	@ApiModelProperty(value = "l_agente", required = true, example = "488")
	private String l_agente;
	@ApiModelProperty(value = "l_fec_from", required = true, example = "15/06/2021")
	private String l_fec_from;
	@ApiModelProperty(value = "l_nom_agt", required = true, example = "CARLOS")
	private String l_nom_agt;
	@ApiModelProperty(value = "l_ape_pat", required = true, example = "GARCIA")
	private String l_ape_pat;
	@ApiModelProperty(value = "pagina", required = true, example = "1")
	private Integer pagina;
	
	
	public Integer getLp_cod_ramo() {
		return lp_cod_ramo;
	}
	public void setLp_cod_ramo(Integer lp_cod_ramo) {
		this.lp_cod_ramo = lp_cod_ramo;
	}
	public Integer getRegistros() {
		return registros;
	}
	public void setRegistros(Integer registros) {
		this.registros = registros;
	}
	public String getTipOrden() {
		return tipOrden;
	}
	public void setTipOrden(String tipOrden) {
		this.tipOrden = tipOrden;
	}
	public String getL_num_contrato() {
		return l_num_contrato;
	}
	public void setL_num_contrato(String l_num_contrato) {
		this.l_num_contrato = l_num_contrato;
	}
	public String getL_fec_to() {
		return l_fec_to;
	}
	public void setL_fec_to(String l_fec_to) {
		this.l_fec_to = l_fec_to;
	}
	public String getL_num_aviso_pago() {
		return l_num_aviso_pago;
	}
	public void setL_num_aviso_pago(String l_num_aviso_pago) {
		this.l_num_aviso_pago = l_num_aviso_pago;
	}
	public Integer getTipConsulta() {
		return tipConsulta;
	}
	public void setTipConsulta(Integer tipConsulta) {
		this.tipConsulta = tipConsulta;
	}
	public Integer getOrdenar() {
		return ordenar;
	}
	public void setOrdenar(Integer ordenar) {
		this.ordenar = ordenar;
	}
	public String getL_agente() {
		return l_agente;
	}
	public void setL_agente(String l_agente) {
		this.l_agente = l_agente;
	}
	public String getL_fec_from() {
		return l_fec_from;
	}
	public void setL_fec_from(String l_fec_from) {
		this.l_fec_from = l_fec_from;
	}
	public String getL_nom_agt() {
		return l_nom_agt;
	}
	public void setL_nom_agt(String l_nom_agt) {
		this.l_nom_agt = l_nom_agt;
	}
	public String getL_ape_pat() {
		return l_ape_pat;
	}
	public void setL_ape_pat(String l_ape_pat) {
		this.l_ape_pat = l_ape_pat;
	}
	public Integer getPagina() {
		return pagina;
	}
	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}
	
	
	
}
