package com.mapfre.tourists.remittance.model.request;

import io.swagger.annotations.ApiModelProperty;

public class PaymentGenerateRequet {

	/**
	 * Objeto que contiene información para realizar la generación de pago por ramo
	 */

	/* Código de ramo */
	@ApiModelProperty(value = "lp_cod_ramo", required = true, example = "405")
	private String lp_cod_ramo;

	/* Código del broker */
	@ApiModelProperty(value = "l_agente", required = true, example = "XMZ0414")
	private String l_agente;

	/* Monto de la prima neta */
	@ApiModelProperty(value = "l_p_neta", required = true, example = "5980")
	private String l_p_neta;

	/* Monto de los impuestos */
	@ApiModelProperty(value = "l_impuestos", required = true, example = "1230")
	private String l_impuestos;

	/* Monto de la comisión */
	@ApiModelProperty(value = "l_comision", required = true, example = "2500")
	private String l_comision;

	/* monto de los derechos del agente */
	@ApiModelProperty(value = "l_dp_agt", required = true, example = "500")
	private String l_dp_agt;

	/* usuario de incio de sesión */
	@ApiModelProperty(value = "usuario", required = true, example = "XMZ0414")
	private String usuario;

	/* nombre del banco */
	@ApiModelProperty(value = "p_banco", required = true, example = "Santander")
	private String p_banco;

	/* número de endoso */
	@ApiModelProperty(value = "l_suplemento", required = true, example = "9801")
	private Integer l_suplemento;

	/* código del agente */
	@ApiModelProperty(value = "l_agt_tur", required = true, example = "XMZ0414")
	private String l_agt_tur;

	/* monto de los derechos de póliza */
	@ApiModelProperty(value = "l_der_pol", required = true, example = "460")
	private String l_der_pol;

	/* monto de la prima total */
	@ApiModelProperty(value = "l_p_total", required = true, example = "6000")
	private String l_p_total;

	/* monto de la sobre comisión */
	@ApiModelProperty(value = "l_sb_comision", required = true, example = "120")
	private String l_sb_comision;

	/* monto de los impuestos del agente */
	@ApiModelProperty(value = "l_imp_agt", required = true, example = "300")
	private String l_imp_agt;

	/* número de póliza */
	@ApiModelProperty(value = "l_Poliza", required = true, example = "398764098")
	private String l_Poliza;

	/* orden */
	@ApiModelProperty(value = "p_orden", required = true, example = "488890")
	private String p_orden;

	public String getLp_cod_ramo() {
		return lp_cod_ramo;
	}

	public String getL_agente() {
		return l_agente;
	}

	public String getL_p_neta() {
		return l_p_neta;
	}

	public String getL_impuestos() {
		return l_impuestos;
	}

	public String getL_comision() {
		return l_comision;
	}

	public String getL_dp_agt() {
		return l_dp_agt;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getP_banco() {
		return p_banco;
	}

	public Integer getL_suplemento() {
		return l_suplemento;
	}

	public String getL_agt_tur() {
		return l_agt_tur;
	}

	public String getL_der_pol() {
		return l_der_pol;
	}

	public String getL_p_total() {
		return l_p_total;
	}

	public String getL_sb_comision() {
		return l_sb_comision;
	}

	public String getL_imp_agt() {
		return l_imp_agt;
	}

	public String getL_Poliza() {
		return l_Poliza;
	}

	public String getP_orden() {
		return p_orden;
	}

	public void setLp_cod_ramo(String lp_cod_ramo) {
		this.lp_cod_ramo = lp_cod_ramo;
	}

	public void setL_agente(String l_agente) {
		this.l_agente = l_agente;
	}

	public void setL_p_neta(String l_p_neta) {
		this.l_p_neta = l_p_neta;
	}

	public void setL_impuestos(String l_impuestos) {
		this.l_impuestos = l_impuestos;
	}

	public void setL_comision(String l_comision) {
		this.l_comision = l_comision;
	}

	public void setL_dp_agt(String l_dp_agt) {
		this.l_dp_agt = l_dp_agt;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setP_banco(String p_banco) {
		this.p_banco = p_banco;
	}

	public void setL_suplemento(Integer l_suplemento) {
		this.l_suplemento = l_suplemento;
	}

	public void setL_agt_tur(String l_agt_tur) {
		this.l_agt_tur = l_agt_tur;
	}

	public void setL_der_pol(String l_der_pol) {
		this.l_der_pol = l_der_pol;
	}

	public void setL_p_total(String l_p_total) {
		this.l_p_total = l_p_total;
	}

	public void setL_sb_comision(String l_sb_comision) {
		this.l_sb_comision = l_sb_comision;
	}

	public void setL_imp_agt(String l_imp_agt) {
		this.l_imp_agt = l_imp_agt;
	}

	public void setL_Poliza(String l_Poliza) {
		this.l_Poliza = l_Poliza;
	}

	public void setP_orden(String p_orden) {
		this.p_orden = p_orden;
	}

}
