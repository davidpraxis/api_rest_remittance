/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.response;

import com.mapfre.tourists.remittance.entity.Condiciones;

public class RecuperaCondicionesResponse {

    private String message;

    private Condiciones condiciones;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Condiciones getCondiciones() {
        return condiciones;
    }

    public void setCondiciones(Condiciones condiciones) {
        this.condiciones = condiciones;
    }
}
