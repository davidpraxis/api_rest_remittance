package com.mapfre.tourists.remittance.model.response;

import java.io.Serializable;
import java.util.List;

public class RecoverRcResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<RecoverPolicyResponse> loc_polizas;
	private List<RecoverAdResponse> loc_aviso;
	private List<RecoverAgentResponse> loc_agente;

	public List<RecoverPolicyResponse> getLoc_polizas() {
		return loc_polizas;
	}

	public void setLoc_polizas(List<RecoverPolicyResponse> loc_polizas) {
		this.loc_polizas = loc_polizas;
	}

	public List<RecoverAdResponse> getLoc_aviso() {
		return loc_aviso;
	}

	public void setLoc_aviso(List<RecoverAdResponse> loc_aviso) {
		this.loc_aviso = loc_aviso;
	}

	public List<RecoverAgentResponse> getLoc_agente() {
		return loc_agente;
	}

	public void setLoc_agente(List<RecoverAgentResponse> loc_agente) {
		this.loc_agente = loc_agente;
	}

}
