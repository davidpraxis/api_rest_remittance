/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.response;

public class AnulaResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
