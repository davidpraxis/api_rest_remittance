package com.mapfre.tourists.remittance.model.response;

import java.io.Serializable;

public class GenerarReporteResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6290821227006011793L;
	
	/*'Código de agente'*/
    private String agentId;
    
    /*'Número de aviso de pago'*/
    private String remittance;
    
    /*'Número de póliza'*/
    private String policy;
    
    /*'Fecha de aplicación'*/
    private String applyDate;
    
    /*'Días'*/
    private Integer days;
    
    /*'Fecha inicio'*/
    private String starts;
    
    /*'Fecha fin'*/
    private String ends;
    
    /*'Monto de la cobertura Physical Damages'*/
    private String R1;
    
    /*'Monto de la cobertura Theft'*/
    private String R2;
    
    /*'Monto de cobertura Medical Expenses'*/
    private String R5;
    
    /*'Monto de cobertura Legal Defense'*/
    private String R6;
    
    /*'Monto de la cobertura Travel Assistance'*/
    private String R7;
    
    /*'Prima neta'*/
    private String netpremium;
    
    /*'Derechos de la póliza'*/
    private String policyfee;
    
    /*'Derechos estatales'*/
    private String federal_tax;
    
    /*'Prima total'*/
    private String total_premium;

    /*'Fecha del remittance'*/
    private String remitdate;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getRemittance() {
		return remittance;
	}

	public void setRemittance(String remittance) {
		this.remittance = remittance;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public String getStarts() {
		return starts;
	}

	public void setStarts(String starts) {
		this.starts = starts;
	}

	public String getEnds() {
		return ends;
	}

	public void setEnds(String ends) {
		this.ends = ends;
	}

	public String getR1() {
		return R1;
	}

	public void setR1(String r1) {
		R1 = r1;
	}

	public String getR2() {
		return R2;
	}

	public void setR2(String r2) {
		R2 = r2;
	}

	public String getR5() {
		return R5;
	}

	public void setR5(String r5) {
		R5 = r5;
	}

	public String getR6() {
		return R6;
	}

	public void setR6(String r6) {
		R6 = r6;
	}

	public String getR7() {
		return R7;
	}

	public void setR7(String r7) {
		R7 = r7;
	}

	public String getNetpremium() {
		return netpremium;
	}

	public void setNetpremium(String netpremium) {
		this.netpremium = netpremium;
	}

	public String getPolicyfee() {
		return policyfee;
	}

	public void setPolicyfee(String policyfee) {
		this.policyfee = policyfee;
	}

	public String getFederal_tax() {
		return federal_tax;
	}

	public void setFederal_tax(String federal_tax) {
		this.federal_tax = federal_tax;
	}

	public String getTotal_premium() {
		return total_premium;
	}

	public void setTotal_premium(String total_premium) {
		this.total_premium = total_premium;
	}

	public String getRemitdate() {
		return remitdate;
	}

	public void setRemitdate(String remitdate) {
		this.remitdate = remitdate;
	}
    
    
}
