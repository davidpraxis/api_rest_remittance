package com.mapfre.tourists.remittance.model;

import java.io.Serializable;

public class RemittanceData implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2286703555344942974L;
	private String remittance;
	private String remittance_date;
	private String status;
	private String agent;
	private Integer net_premium;
	private Integer policy_fee;
	private Integer mexican_tax;
	private Integer prima_total;
	private Integer comission;
	private Integer total;
	
	
	public String getRemittance() {
		return remittance;
	}
	public void setRemittance(String remittance) {
		this.remittance = remittance;
	}
	public String getRemittance_date() {
		return remittance_date;
	}
	public void setRemittance_date(String remittance_date) {
		this.remittance_date = remittance_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public Integer getNet_premium() {
		return net_premium;
	}
	public void setNet_premium(Integer net_premium) {
		this.net_premium = net_premium;
	}
	public Integer getPolicy_fee() {
		return policy_fee;
	}
	public void setPolicy_fee(Integer policy_fee) {
		this.policy_fee = policy_fee;
	}
	public Integer getMexican_tax() {
		return mexican_tax;
	}
	public void setMexican_tax(Integer mexican_tax) {
		this.mexican_tax = mexican_tax;
	}
	public Integer getPrima_total() {
		return prima_total;
	}
	public void setPrima_total(Integer prima_total) {
		this.prima_total = prima_total;
	}
	public Integer getComission() {
		return comission;
	}
	public void setComission(Integer comission) {
		this.comission = comission;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	

}
