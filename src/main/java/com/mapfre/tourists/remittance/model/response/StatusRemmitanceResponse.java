/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 25/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.response;

import com.mapfre.tourists.remittance.entity.StatusRemmitance;

public class StatusRemmitanceResponse {

    private String message;
    private StatusRemmitance remmitance;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StatusRemmitance getRemmitance() {
        return remmitance;
    }

    public void setRemmitance(StatusRemmitance remmitance) {
        this.remmitance = remmitance;
    }
}
