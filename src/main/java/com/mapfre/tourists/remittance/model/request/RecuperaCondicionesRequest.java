/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Cadena con los datos mínimos para RECUPERAR CONDICIONES DEL SUB AGENTE CON ROL AGENTE SIN REMMITANCE")
public class RecuperaCondicionesRequest {

    /*
        #1
        description: Código de la compañia
     */
    @ApiModelProperty(value = "cmpVal", required = true, example = "1")
    private String cmpVal;

    /*
        #2
        description: Código de usuario
    */
    @ApiModelProperty(value = "usrVal", required = true, example = "albertoj")
    private String usrVal;

    /*
        #3
        description: Idioma (ES)
    */
    @ApiModelProperty(value = "lngVal", required = true, example = "ES")
    private String lngVal;

    /*
        #4
        description: código del agente
    */
    @ApiModelProperty(value = "p_agente", required = true, example = "99998")
    private String p_agente;

    /*
        #5
        description: "número de contrato\r\n"
    */
    @ApiModelProperty(value = "p_contrato", required = true, example = "19036")
    private String p_contrato;

    /*
        #6
        description: "código de ramo\r\n"
    */
    @ApiModelProperty(value = "p_cod_ramo", required = true, example = "500")
    private String p_cod_ramo;

    /*
        #7
        description: "número de cantidad de pólizas\r\n"
    */
    @ApiModelProperty(value = "p_polizas", required = true, example = "2")
    private String p_polizas;

    /*
        #8
        description: "números de pólizas\r\n"
    */
    @ApiModelProperty(value = "p_cadena_polizas", required = true, example = "5002100000046,5002100000047")
    private String p_cadena_polizas;

    /*
        #9
        description: "prima neta\r\n"
    */
    @ApiModelProperty(value = "p_prima_neta", required = true, example = "500000")
    private String p_prima_neta;

    /*
        #10
        description: "derechos de póliza\r\n"
    */
    @ApiModelProperty(value = "p_der_poliza", required = true, example = "250")
    private String p_der_poliza;

    /*
        #11
        description: "prima total\r\n"
    */
    @ApiModelProperty(value = "p_prima_total", required = true, example = "700000")
    private String p_prima_total;

    public String getCmpVal() {
        return cmpVal;
    }

    public void setCmpVal(String cmpVal) {
        this.cmpVal = cmpVal;
    }

    public String getUsrVal() {
        return usrVal;
    }

    public void setUsrVal(String usrVal) {
        this.usrVal = usrVal;
    }

    public String getLngVal() {
        return lngVal;
    }

    public void setLngVal(String lngVal) {
        this.lngVal = lngVal;
    }

    public String getP_agente() {
        return p_agente;
    }

    public void setP_agente(String p_agente) {
        this.p_agente = p_agente;
    }

    public String getP_contrato() {
        return p_contrato;
    }

    public void setP_contrato(String p_contrato) {
        this.p_contrato = p_contrato;
    }

    public String getP_cod_ramo() {
        return p_cod_ramo;
    }

    public void setP_cod_ramo(String p_cod_ramo) {
        this.p_cod_ramo = p_cod_ramo;
    }

    public String getP_polizas() {
        return p_polizas;
    }

    public void setP_polizas(String p_polizas) {
        this.p_polizas = p_polizas;
    }

    public String getP_cadena_polizas() {
        return p_cadena_polizas;
    }

    public void setP_cadena_polizas(String p_cadena_polizas) {
        this.p_cadena_polizas = p_cadena_polizas;
    }

    public String getP_prima_neta() {
        return p_prima_neta;
    }

    public void setP_prima_neta(String p_prima_neta) {
        this.p_prima_neta = p_prima_neta;
    }

    public String getP_der_poliza() {
        return p_der_poliza;
    }

    public void setP_der_poliza(String p_der_poliza) {
        this.p_der_poliza = p_der_poliza;
    }

    public String getP_prima_total() {
        return p_prima_total;
    }

    public void setP_prima_total(String p_prima_total) {
        this.p_prima_total = p_prima_total;
    }
}
