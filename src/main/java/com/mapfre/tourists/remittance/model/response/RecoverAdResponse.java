package com.mapfre.tourists.remittance.model.response;

import java.io.Serializable;

public class RecoverAdResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer cod_cia;
	private Integer cod_sector;
	private Integer cod_ramo;
	private Integer num_aviso_pago;
	private String fec_aviso_pago;
	private String cod_agt_tur;
	private String estatus;
	private String prima_neta;
	private String derecho_poliza;
	private String impuestos;
	private String prima_total;
	private String comision;
	private String sobre_comision;
	private String derecho_pol_agente;
	private String impuesto_agente;
	private String fec_validez;
	private String cod_usr;
	private String fec_actu;
	private String nom_banco;
	private Integer num_orden;
	private String cod_agt_broker;
	private Integer num_secu;

	public Integer getCod_cia() {
		return cod_cia;
	}

	public void setCod_cia(Integer cod_cia) {
		this.cod_cia = cod_cia;
	}

	public Integer getCod_sector() {
		return cod_sector;
	}

	public void setCod_sector(Integer cod_sector) {
		this.cod_sector = cod_sector;
	}

	public Integer getCod_ramo() {
		return cod_ramo;
	}

	public void setCod_ramo(Integer cod_ramo) {
		this.cod_ramo = cod_ramo;
	}

	public Integer getNum_aviso_pago() {
		return num_aviso_pago;
	}

	public void setNum_aviso_pago(Integer num_aviso_pago) {
		this.num_aviso_pago = num_aviso_pago;
	}

	public String getFec_aviso_pago() {
		return fec_aviso_pago;
	}

	public void setFec_aviso_pago(String fec_aviso_pago) {
		this.fec_aviso_pago = fec_aviso_pago;
	}

	public String getCod_agt_tur() {
		return cod_agt_tur;
	}

	public void setCod_agt_tur(String cod_agt_tur) {
		this.cod_agt_tur = cod_agt_tur;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getPrima_neta() {
		return prima_neta;
	}

	public void setPrima_neta(String prima_neta) {
		this.prima_neta = prima_neta;
	}

	public String getDerecho_poliza() {
		return derecho_poliza;
	}

	public void setDerecho_poliza(String derecho_poliza) {
		this.derecho_poliza = derecho_poliza;
	}

	public String getImpuestos() {
		return impuestos;
	}

	public void setImpuestos(String impuestos) {
		this.impuestos = impuestos;
	}

	public String getPrima_total() {
		return prima_total;
	}

	public void setPrima_total(String prima_total) {
		this.prima_total = prima_total;
	}

	public String getComision() {
		return comision;
	}

	public void setComision(String comision) {
		this.comision = comision;
	}

	public String getSobre_comision() {
		return sobre_comision;
	}

	public void setSobre_comision(String sobre_comision) {
		this.sobre_comision = sobre_comision;
	}

	public String getDerecho_pol_agente() {
		return derecho_pol_agente;
	}

	public void setDerecho_pol_agente(String derecho_pol_agente) {
		this.derecho_pol_agente = derecho_pol_agente;
	}

	public String getImpuesto_agente() {
		return impuesto_agente;
	}

	public void setImpuesto_agente(String impuesto_agente) {
		this.impuesto_agente = impuesto_agente;
	}

	public String getFec_validez() {
		return fec_validez;
	}

	public void setFec_validez(String fec_validez) {
		this.fec_validez = fec_validez;
	}

	public String getCod_usr() {
		return cod_usr;
	}

	public void setCod_usr(String cod_usr) {
		this.cod_usr = cod_usr;
	}

	public String getFec_actu() {
		return fec_actu;
	}

	public void setFec_actu(String fec_actu) {
		this.fec_actu = fec_actu;
	}

	public String getNom_banco() {
		return nom_banco;
	}

	public void setNom_banco(String nom_banco) {
		this.nom_banco = nom_banco;
	}

	public Integer getNum_orden() {
		return num_orden;
	}

	public void setNum_orden(Integer num_orden) {
		this.num_orden = num_orden;
	}

	public String getCod_agt_broker() {
		return cod_agt_broker;
	}

	public void setCod_agt_broker(String cod_agt_broker) {
		this.cod_agt_broker = cod_agt_broker;
	}

	public Integer getNum_secu() {
		return num_secu;
	}

	public void setNum_secu(Integer num_secu) {
		this.num_secu = num_secu;
	}

}
