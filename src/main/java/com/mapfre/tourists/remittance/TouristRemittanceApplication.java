package com.mapfre.tourists.remittance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TouristRemittanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TouristRemittanceApplication.class, args);
	}

}
