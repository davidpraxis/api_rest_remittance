/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.RecuperaCondicionesRequest;
import com.mapfre.tourists.remittance.model.response.RecuperaCondicionesResponse;
import org.springframework.stereotype.Service;

@Service
public interface RecuperaCondicionesService {

    /**
     * Operación que recupera condiciones del sub agente con rol agente sin remittance
     */
    public RecuperaCondicionesResponse recuperaCondicionesMock(RecuperaCondicionesRequest request);
}
