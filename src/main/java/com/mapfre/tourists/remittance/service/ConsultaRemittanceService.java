package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.ConsultaRemittanceRequest;
import com.mapfre.tourists.remittance.model.response.ConsultaRemittanceResponse;

public interface ConsultaRemittanceService {
	
	public ConsultaRemittanceResponse consultaRemittanceMock(ConsultaRemittanceRequest request);
	
	public ConsultaRemittanceResponse consultaRemittance(ConsultaRemittanceRequest request);

}
