package com.mapfre.tourists.remittance.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapfre.tourists.remittance.model.RemittanceData;
import com.mapfre.tourists.remittance.model.request.ConsultaRemittanceRequest;
import com.mapfre.tourists.remittance.model.response.ConsultaRemittanceResponse;
import com.mapfre.tourists.remittance.repository.IConsultaRemittanceRepository;
import com.mapfre.tourists.remittance.service.ConsultaRemittanceService;

@Service
public class ConsultaRemittanceServiceImpl implements ConsultaRemittanceService {

	private static final Logger LOGGER = LogManager.getLogger(ConsultaRemittanceServiceImpl.class);
	
	
	 @Autowired
	 IConsultaRemittanceRepository consultaRemittanceRepository;


	public ConsultaRemittanceResponse consultaRemittanceMock(ConsultaRemittanceRequest request) {

		ConsultaRemittanceResponse response = new ConsultaRemittanceResponse();
		
		
		List<RemittanceData> consulta = new ArrayList<RemittanceData>();
		
		
		RemittanceData data1 = new RemittanceData();
		RemittanceData data2 = new RemittanceData();
		
		data1.setRemittance("404");
		data1.setRemittance_date("25/05/2022");
		data1.setStatus("ACTIVO");
		data1.setAgent("CARLOS");
		data1.setNet_premium(2342);
		data1.setPolicy_fee(34546456);
		data1.setMexican_tax(5676);
		data1.setPrima_total(94863294);
		data1.setComission(5678);
		data1.setTotal(67867);
		
		data2.setRemittance("405");
		data2.setRemittance_date("20/05/2022");
		data2.setStatus("ACTIVO");
		data2.setAgent("PEREZ");
		data2.setNet_premium(456);
		data2.setPolicy_fee(56756);
		data2.setMexican_tax(890);
		data2.setPrima_total(134245645);
		data2.setComission(567);
		data2.setTotal(78564);
		
		consulta.add(data1);
		consulta.add(data2);
		
		response.setConsulta(consulta);
		
		return response;
	}
	
	public ConsultaRemittanceResponse consultaRemittance(ConsultaRemittanceRequest request) {
		
		
		consultaRemittanceRepository.consultaRemittance(null, null, null, null, null, null, null, null, null, null, null, null, null);
		
		ConsultaRemittanceResponse response = new ConsultaRemittanceResponse();
		
		
		
		return response;
	}


}
