/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 26/01/2022, miércoles
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service.impl;

import com.mapfre.tourists.remittance.entity.StatusRemmitance;
import com.mapfre.tourists.remittance.model.request.StatusRemmitanceRequest;
import com.mapfre.tourists.remittance.model.response.StatusRemmitanceResponse;
import com.mapfre.tourists.remittance.service.StatusRemmitanceService;
import com.mapfre.tourists.remittance.utils.NumberFormatUtil;
import static com.mapfre.tourists.remittance.utils.constants.StatusConstants.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class StatusRemmitanceServiceImpl implements StatusRemmitanceService {

    private static final Logger logger = LogManager.getLogger(StatusRemmitanceServiceImpl.class);

    @Override
    public StatusRemmitanceResponse getStatusRemmitanceMock(StatusRemmitanceRequest request) {

        StatusRemmitanceResponse response = null;
        try {
            if(validEntry(request)){
                response = validResponseMock(request);
                if(response == null){
                    response = invalidResponseMock(request);
                }
            }else{
                throw new Exception(Errors.ERROR_DATA);
            }
        }catch(Exception e){
            logger.info(e.getMessage());
        }
        return response;
    }

    public StatusRemmitanceResponse validResponseMock(StatusRemmitanceRequest request){
        StatusRemmitanceResponse response = new StatusRemmitanceResponse();
        StatusRemmitance remmitance= new StatusRemmitance();
        remmitance.setCod_status("2"); //(2,3,7);
        remmitance.setNom_status("Activo");

        response.setMessage(OPERATION_SUCCESS);
        response.setRemmitance(remmitance);
        return response;
    }

    public StatusRemmitanceResponse invalidResponseMock(StatusRemmitanceRequest request){
        StatusRemmitanceResponse response = new StatusRemmitanceResponse();
        StatusRemmitance remmitance= null;

        response.setMessage(OPERATION_NOT_SUCCESS);
        response.setRemmitance(remmitance);
        return response;
    }



    public Boolean validEntry(StatusRemmitanceRequest request) {
        Boolean existeNodo = true;
        String nodo = "";
        NumberFormatUtil formatUtil = new NumberFormatUtil();

        nodo = DatosMinimos.CODE_RAMO;
        try {
            if (request.getRamo() == null || request.getRamo() <= 0) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }
        } catch (Exception ex) {
            logger.info(ex);
            existeNodo = false;
        }

        nodo = DatosMinimos.CMP_VAL;
        try {
            if (request.getCmpVal() == null || request.getCmpVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer cmpV= formatUtil.convertToInteger(request.getCmpVal());
                if(cmpV <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+ nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
            existeNodo = false;
        }

        nodo = DatosMinimos.USR_VAL;
        try {
            if (request.getCmpVal() == null || request.getCmpVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }/*else{
                Integer cmpV= formatUtil.convertToInteger(request.getCmpVal());
                if(cmpV <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+ nodo);
                }
            }*/
        } catch (Exception ex) {
            logger.info(ex);
            existeNodo = false;
        }

        nodo = DatosMinimos.LGN_VAL;
        try {
            if (request.getCmpVal() == null || request.getCmpVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }
        } catch (Exception ex) {
            logger.info(ex);
            existeNodo = false;
        }

        return existeNodo;
    }
}
