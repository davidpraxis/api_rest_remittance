package com.mapfre.tourists.remittance.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapfre.tourists.remittance.exception.ParameterException;
import com.mapfre.tourists.remittance.model.request.PaymentGenerateRequet;
import com.mapfre.tourists.remittance.model.request.RecoverRcRequest;
import com.mapfre.tourists.remittance.model.response.PaymentGenerateResponse;
import com.mapfre.tourists.remittance.model.response.RecoverAdResponse;
import com.mapfre.tourists.remittance.model.response.RecoverAgentResponse;
import com.mapfre.tourists.remittance.model.response.RecoverPolicyResponse;
import com.mapfre.tourists.remittance.model.response.RecoverRcResponse;
import com.mapfre.tourists.remittance.repository.IPaymentGenerateRepository;
import com.mapfre.tourists.remittance.repository.IRecoverRcRepository;
import com.mapfre.tourists.remittance.service.PaymentGenerateService;


@Service
public class PaymentGenerateServiceImpl implements PaymentGenerateService {

	@Autowired
	private IPaymentGenerateRepository paymentRepository;

	private static final Logger LOGGER = LogManager.getLogger(PaymentGenerateServiceImpl.class);

	@Override
	public PaymentGenerateResponse generatePaymentMock(PaymentGenerateRequet request) {
		
		PaymentGenerateResponse response = new PaymentGenerateResponse();
		int min = 10;
		int max = 1000000;
		response.setL_resultado(String.valueOf((int)Math.floor(Math.random()*(max-min+1)+min)));
		return response;
	}
	
	@Override
	public PaymentGenerateResponse generatePayment(PaymentGenerateRequet request) {
		
		paymentRepository.paymentGenerate(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
		
		PaymentGenerateResponse response = new PaymentGenerateResponse();
		return response;
	}

}
