package com.mapfre.tourists.remittance.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapfre.tourists.remittance.model.request.GenerarReporteRequest;
import com.mapfre.tourists.remittance.model.response.GenerarReporteResponse;
import com.mapfre.tourists.remittance.repository.IGenerarReporteRepository;
import com.mapfre.tourists.remittance.service.GenerarReporteService;

@Service
public class GenerarReporteServiceImpl implements GenerarReporteService {
	
	@Autowired
	private IGenerarReporteRepository generarReporteRepository;

	@Override
	public GenerarReporteResponse generarReporteMock(GenerarReporteRequest request) {
		
		GenerarReporteResponse response = new GenerarReporteResponse();
		response.setAgentId("123345");
		response.setApplyDate("26/01/22");
		response.setDays(5);
		response.setEnds("31/01/2022");
		response.setFederal_tax("2548555");
		response.setNetpremium("568745");
		response.setPolicy("21515");
		response.setPolicyfee("AASRG");
		response.setR1("AE");
		response.setR2("AE");
		response.setR5("AE");
		response.setR6("AE");
		response.setR7("AE");
		response.setRemitdate("26/01/2020");
		response.setRemittance("23543");
		response.setStarts("26/01/2022");
		response.setTotal_premium("21334");
		return response;
	}

	@Override
	public GenerarReporteResponse generarReporte(GenerarReporteRequest request) {
		GenerarReporteResponse response = new GenerarReporteResponse();
		
		generarReporteRepository.generarReporteRemittance(null, null, null, null, null, null, null, null, null);
		
		return response;
	}

}
