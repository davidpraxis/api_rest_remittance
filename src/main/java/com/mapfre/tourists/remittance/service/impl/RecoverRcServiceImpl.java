package com.mapfre.tourists.remittance.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapfre.tourists.remittance.model.request.RecoverRcRequest;
import com.mapfre.tourists.remittance.model.response.RecoverAdResponse;
import com.mapfre.tourists.remittance.model.response.RecoverAgentResponse;
import com.mapfre.tourists.remittance.model.response.RecoverPolicyResponse;
import com.mapfre.tourists.remittance.model.response.RecoverRcResponse;
import com.mapfre.tourists.remittance.repository.IRecoverRcRepository;
import com.mapfre.tourists.remittance.service.RecoverRcService;

@Service
public class RecoverRcServiceImpl implements RecoverRcService {

	@Autowired
	IRecoverRcRepository recoverRcRepository;

	private static final Logger LOGGER = LogManager.getLogger(RecoverRcServiceImpl.class);
	
	@Override
	public RecoverRcResponse recoverRcMock(RecoverRcRequest request) {

		RecoverRcResponse response = new RecoverRcResponse();

		List<RecoverPolicyResponse> policy = new ArrayList<RecoverPolicyResponse>();
		List<RecoverAdResponse> ad = new ArrayList<RecoverAdResponse>();
		List<RecoverAgentResponse> agent = new ArrayList<RecoverAgentResponse>();

		RecoverPolicyResponse policyData1 = new RecoverPolicyResponse();
		RecoverPolicyResponse policyData2 = new RecoverPolicyResponse();

		RecoverAdResponse adData1 = new RecoverAdResponse();
		RecoverAdResponse adData2 = new RecoverAdResponse();

		RecoverAgentResponse agentData1 = new RecoverAgentResponse();
		RecoverAgentResponse agentData2 = new RecoverAgentResponse();

		policyData1.setPolicy("0126895099064");
		policyData1.setFecha("12/04/2022");
		policyData1.setPremium("53");
		policyData1.setPolicy_fee("34546456");
		policyData1.setTax("16");
		policyData1.setTotal("4985");
		policyData1.setR6(998);
		policyData1.setR7(268);
		policy.add(policyData1);

		policyData2.setPolicy("0483726409002");
		policyData2.setFecha("10/03/2022");
		policyData2.setPremium("5345");
		policyData2.setPolicy_fee("11139082");
		policyData2.setTax("99");
		policyData2.setTotal("3467");
		policyData2.setR6(123);
		policyData2.setR7(321);
		policy.add(policyData2);

		adData1.setCod_cia(129);
		adData1.setCod_sector(61);
		adData1.setCod_ramo(2001);
		adData1.setNum_aviso_pago(1589);
		adData1.setFec_aviso_pago("25/01/2022");
		adData1.setCod_agt_tur("A2001");
		adData1.setEstatus("ACTIVO");
		adData1.setPrima_neta("60");
		adData1.setDerecho_poliza("A123B");
		adData1.setImpuestos("1080");
		adData1.setPrima_total("6987");
		adData1.setComision("3999");
		adData1.setSobre_comision("769");
		adData1.setDerecho_pol_agente("1234");
		adData1.setImpuesto_agente("290");
		adData1.setFec_validez("31/12/2024");
		adData1.setCod_usr("XMZ0414");
		adData1.setFec_actu("25/01/2020");
		adData1.setNom_banco("BBVA");
		adData1.setNum_orden(78956);
		adData1.setCod_agt_broker("XMAS156");
		adData1.setNum_secu(18764537);
		ad.add(adData1);

		adData2.setCod_cia(130);
		adData2.setCod_sector(16);
		adData2.setCod_ramo(2001234);
		adData2.setNum_aviso_pago(5583);
		adData2.setFec_aviso_pago("01/02/2022");
		adData2.setCod_agt_tur("00121390");
		adData2.setEstatus("ACTIVO");
		adData2.setPrima_neta("350");
		adData2.setDerecho_poliza("A12PB");
		adData2.setImpuestos("1080");
		adData2.setPrima_total("6987");
		adData2.setComision("3999");
		adData2.setSobre_comision("769");
		adData2.setDerecho_pol_agente("1234");
		adData2.setImpuesto_agente("290");
		adData2.setFec_validez("31/12/2024");
		adData2.setCod_usr("XMZ0414");
		adData2.setFec_actu("25/01/2020");
		adData2.setNom_banco("BBVA");
		adData2.setNum_orden(78956);
		adData2.setCod_agt_broker("XMAS156");
		adData2.setNum_secu(18764537);
		ad.add(adData2);

		agentData1.setNombre("Antonio Huertas");
		agentData1.setClave("A12309");
		agentData1.setEmail("test@test.com");
		agentData1.setDomicilio("C. Río Tiber, Cuauhtémoc");
		agentData1.setTelefono("5552307120");
		agentData1.setCiudad("Ciudad de México, CDMX");
		agent.add(agentData1);

		agentData2.setNombre("Esteban Tejera");
		agentData2.setClave("B0001");
		agentData2.setEmail("test@test.com");
		agentData2.setDomicilio("C. Río Tiber, Cuauhtémoc");
		agentData2.setTelefono("5552307120");
		agentData2.setCiudad("Ciudad de México, CDMX");
		agent.add(agentData2);

		response.setLoc_polizas(policy);
		response.setLoc_aviso(ad);
		response.setLoc_agente(agent);

		return response;
	}
	
	@Override
	public RecoverRcResponse recoverRc(RecoverRcRequest request) {
		recoverRcRepository.recoverRc(request.getlRemittance(), request.getLpCodRamo());

		RecoverRcResponse response = new RecoverRcResponse();

		return response;
	}

}
