/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service.impl;

import com.mapfre.tourists.remittance.model.request.AnulaRequest;
import com.mapfre.tourists.remittance.model.response.AnulaResponse;
import com.mapfre.tourists.remittance.repository.AnulaRepository;
import com.mapfre.tourists.remittance.service.AnulaService;
import com.mapfre.tourists.remittance.utils.constants.AnulaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static com.mapfre.tourists.remittance.utils.constants.AnulaConstants.*;

@Service
public class AnulaServiceImpl implements AnulaService {

    @Autowired
    AnulaRepository anulaRepository;

    @Override
    public AnulaResponse cancelPolicy(AnulaRequest request) {
        AnulaResponse response = new AnulaResponse();
        try {
            String res = anulaRepository.cancelPolicy(request.getL_cod_ramo(), request.getL_aviso_paso());
            
            if(request.getL_cod_ramo() != null && request.getL_cod_ramo() >0
                    && request.getL_aviso_paso() != null && request.getL_aviso_paso() > 0){
                if(request.getL_cod_ramo() == 500 ||request.getL_aviso_paso() == 501){
                    response.setMessage(res);
                }else{
                    response.setMessage(NOT_SUCCESS_MESSAGE);
                }
            }else{
                throw new Exception("400 Error data, please introduce valid data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public AnulaResponse cancelPolicyMock(AnulaRequest request) {
        AnulaResponse response = null;
        try {
            if(request.getL_cod_ramo() != null && request.getL_cod_ramo() >0
                    && request.getL_aviso_paso() != null && request.getL_aviso_paso() > 0){
                if(request.getL_cod_ramo() == 500 ||request.getL_aviso_paso() == 501){
                    response = cancelPolicyWithValidEntry(request);
                }else{
                    response = cancelPolicyWithInvalidEntry(request);
                }
            }else{
                throw new Exception("400 Error data, please introduce valid data");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }


    public AnulaResponse cancelPolicyWithValidEntry(AnulaRequest request) {
        AnulaResponse response = new AnulaResponse();
        response.setMessage(SUCCESS_MESSAGE);
        return response;
    }

    public AnulaResponse cancelPolicyWithInvalidEntry(AnulaRequest request) {
        AnulaResponse response = new AnulaResponse();
        response.setMessage(NOT_SUCCESS_MESSAGE);
        return response;
    }
}
