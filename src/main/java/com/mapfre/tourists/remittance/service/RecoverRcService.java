package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.RecoverRcRequest;
import com.mapfre.tourists.remittance.model.response.RecoverRcResponse;


public interface RecoverRcService {

	public RecoverRcResponse recoverRcMock(RecoverRcRequest request);

	public RecoverRcResponse recoverRc(RecoverRcRequest request);

}
