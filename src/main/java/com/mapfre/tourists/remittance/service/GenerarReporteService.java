package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.GenerarReporteRequest;
import com.mapfre.tourists.remittance.model.response.GenerarReporteResponse;

public interface GenerarReporteService {
	
	public GenerarReporteResponse generarReporteMock(GenerarReporteRequest request);
	
	public GenerarReporteResponse generarReporte(GenerarReporteRequest request);

}
