package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.PaymentGenerateRequet;
import com.mapfre.tourists.remittance.model.response.PaymentGenerateResponse;


public interface PaymentGenerateService {

	public PaymentGenerateResponse generatePaymentMock(PaymentGenerateRequet request);

	public PaymentGenerateResponse generatePayment(PaymentGenerateRequet request);

}
