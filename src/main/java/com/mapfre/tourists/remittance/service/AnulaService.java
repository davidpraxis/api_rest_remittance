/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.AnulaRequest;
import com.mapfre.tourists.remittance.model.response.AnulaResponse;
import org.springframework.stereotype.Service;

@Service
public interface AnulaService {

    /**
     * Proceso para anular Polizas
     * @param request
     * @return
     */
    public AnulaResponse cancelPolicy(AnulaRequest request);

    public AnulaResponse cancelPolicyMock(AnulaRequest request);
}
