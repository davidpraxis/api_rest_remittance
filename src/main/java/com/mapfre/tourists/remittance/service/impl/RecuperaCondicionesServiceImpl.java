/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service.impl;

import com.mapfre.tourists.remittance.entity.Condiciones;
import com.mapfre.tourists.remittance.model.request.RecuperaCondicionesRequest;
import com.mapfre.tourists.remittance.model.response.RecuperaCondicionesResponse;
import com.mapfre.tourists.remittance.service.RecuperaCondicionesService;
import com.mapfre.tourists.remittance.utils.NumberFormatUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import static com.mapfre.tourists.remittance.utils.constants.CondicionesConstants.*;

@Service
public class RecuperaCondicionesServiceImpl implements RecuperaCondicionesService {

    private static final Logger logger = LogManager.getLogger(RecuperaCondicionesServiceImpl.class);

    @Override
    public RecuperaCondicionesResponse recuperaCondicionesMock(RecuperaCondicionesRequest request) {

        RecuperaCondicionesResponse response = null;

        try {
            /**
             * Valida todos los campos
             */
            if(validEntry(request)){
                response = recuperaCondicionesDatosValidos(request);
                if(response == null){
                    response = recuperaCondicionesDatosInvalidos(request);
                }
            }else{
                throw new Exception("400 Error data, please introduce valid data");
            }
        }catch(Exception e){
            logger.info(e.getMessage());
        }
        return response;
    }

    public RecuperaCondicionesResponse recuperaCondicionesDatosValidos(RecuperaCondicionesRequest request){

        RecuperaCondicionesResponse response = new RecuperaCondicionesResponse();
        NumberFormatUtil formatUtil= new NumberFormatUtil();

        /**
         * SE AGREGAN TEMPORALMENTE ESTOS CALCULOS, YA QUE SE DESCONOCE LOS DATOS REALES
         */
        Double l_num_comision= CondicionesTest.L_NUM_COMISION;
        Double l_num_sobrecomision = CondicionesTest.L_NUM_SOBRECOMISION;
        Double l_der_pol_agt_sal = CondicionesTest.L_DER_POL_AGT_SAL;
        Double l_der_pol_agt = CondicionesTest.L_DER_POL_AGT;

        Double p_prima_neta = formatUtil.convertToDouble(request.getP_prima_neta());
        Double p_prima_total = formatUtil.convertToDouble(request.getP_prima_total());
        Double p_der_poliza = formatUtil.convertToDouble(request.getP_der_poliza());

        //se calcula la comisi«n y sobrecomisi«n
        Double l_num_comision_sal = ((l_num_comision/100) * p_prima_neta) * -1;
        Double l_num_sobrecomision_sal  = ((l_num_sobrecomision/100) * p_prima_neta) * -1 ;
        l_der_pol_agt_sal = ((l_der_pol_agt / 100) * p_der_poliza) * -1;
        Double l_remittance_total_sal = p_prima_total + (l_der_pol_agt_sal + l_num_comision_sal + l_num_sobrecomision_sal);

        /**  ############################################################################## */
        Condiciones condiciones = new Condiciones();

        condiciones.setNum_comision_sal(l_num_comision_sal);
        condiciones.setNum_sobrecomision_sal(l_num_sobrecomision_sal);
        condiciones.setDer_pol_agt_sal(l_der_pol_agt_sal);
        condiciones.setRemittance_total_sal(l_remittance_total_sal);

        response.setMessage(OPERATION_SUCCESS);
        response.setCondiciones(condiciones);
        return response;
    }

    public RecuperaCondicionesResponse recuperaCondicionesDatosInvalidos(RecuperaCondicionesRequest request){
        RecuperaCondicionesResponse response = new RecuperaCondicionesResponse();
        Condiciones condiciones = new Condiciones();

        logger.info(Errors.L_ERROR+" | "+
                    Errors.L_ERROR_RANGO+" | "+
                    Errors.L_ERROR_MON_AB+" | "+
                    Errors.L_EXISTE+" | ");
        response.setMessage(OPERATION_NOT_SUCCESS);
        response.setCondiciones(condiciones);

        return response;
    }

    public Boolean validEntry(RecuperaCondicionesRequest request){
        Boolean existeNodo = false;
        String nodo="";
        NumberFormatUtil formatUtil= new NumberFormatUtil();

        nodo = DatosMinimos.CMP_VAL;
        try {
            if (request.getCmpVal() == null || request.getCmpVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer cmpV= formatUtil.convertToInteger(request.getCmpVal());
                if(cmpV <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+ nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.USR_VAL;
        try {
            if (request.getUsrVal() == null || request.getUsrVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.LGN_VAL;
        try {
            if (request.getLngVal() == null || request.getLngVal().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_AGENTE;
        try {
            if (request.getP_agente() == null || request.getP_agente().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer codeAgent= formatUtil.convertToInteger(request.getP_agente());
                if(codeAgent <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_CONTRATO;
        try {
            if (request.getP_contrato() == null || request.getP_contrato().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer contract = formatUtil.convertToInteger(request.getP_contrato());
                if(contract <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_COD_RAMO;
        try {
            if (request.getP_cod_ramo() == null || request.getP_cod_ramo().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer codRamo = formatUtil.convertToInteger(request.getP_cod_ramo());
                if(codRamo <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_POLIZAS;
        try {
            if (request.getP_polizas() == null || request.getP_polizas().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer numPolicys = formatUtil.convertToInteger(request.getP_polizas());
                if(numPolicys <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_INVALID+nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_CADENA_POLIZAS;
        try {
            if (request.getP_cadena_polizas() == null || request.getP_cadena_polizas().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_PRIMA_NETA;
        try {
            if (request.getP_prima_neta() == null || request.getP_prima_neta().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Double primaNeta = formatUtil.convertToDouble(request.getP_prima_neta());
                if(primaNeta <= 0 ){
                    throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
                }
            }

        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_DER_POLIZA;
        try {
            if (request.getP_der_poliza() == null || request.getP_der_poliza().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else{
                Integer derPolicy = formatUtil.convertToInteger(request.getP_der_poliza());
                if(derPolicy <= 0){
                    throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }

        nodo = DatosMinimos.P_PRIMA_TOTAL;
        try {
            if (request.getP_prima_total() == null || request.getP_prima_total().isEmpty()) {
                throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
            }else {
                Double primaTotal = formatUtil.convertToDouble(request.getP_prima_total());
                if (primaTotal <= 0) {
                    throw new Exception(Errors.ERROR_CAMPO_EMPTY + nodo);
                }
            }
        } catch (Exception ex) {
            logger.info(ex);
        }
        existeNodo = true;
        return existeNodo;
    }
}
