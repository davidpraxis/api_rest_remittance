/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 26/01/2022, miércoles
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.service;

import com.mapfre.tourists.remittance.model.request.StatusRemmitanceRequest;
import com.mapfre.tourists.remittance.model.response.StatusRemmitanceResponse;
import org.springframework.stereotype.Service;

@Service
public interface StatusRemmitanceService {

    /**
     * Creacion. ESTATUS REMITTANCE MOCK
     * @param request
     * @return
     */
    public StatusRemmitanceResponse getStatusRemmitanceMock(StatusRemmitanceRequest request);
}
