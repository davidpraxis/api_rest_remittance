package com.mapfre.tourists.remittance.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	public LocalDateTimeSerializer() {
		this(null);
	}

	public LocalDateTimeSerializer(final Class<LocalDateTime> t) {
		super(t);
	}

	@Override
	public void serialize(final LocalDateTime value, final JsonGenerator generator, final SerializerProvider provider)
			throws IOException {
		generator.writeString(dtf.format(value));
	}

}
