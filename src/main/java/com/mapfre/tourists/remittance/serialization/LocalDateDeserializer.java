package com.mapfre.tourists.remittance.serialization;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class LocalDateDeserializer extends StdDeserializer<LocalDate> {

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public LocalDateDeserializer() {
		this(null);
	}

	public LocalDateDeserializer(final Class<LocalDate> t) {
		super(t);
	}

	@Override
	public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException {
				JsonNode node = p.getCodec().readTree(p);
				return LocalDate.parse(node.textValue(), dtf);
	}

}
