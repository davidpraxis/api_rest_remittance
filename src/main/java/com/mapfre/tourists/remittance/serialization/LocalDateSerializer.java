package com.mapfre.tourists.remittance.serialization;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LocalDateSerializer extends StdSerializer<LocalDate> {

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public LocalDateSerializer() {
		this(null);
	}

	public LocalDateSerializer(final Class<LocalDate> t) {
		super(t);
	}

	@Override
	public void serialize(final LocalDate value, final JsonGenerator generator, final SerializerProvider provider)
			throws IOException {
		generator.writeString(dtf.format(value));
	}

}
