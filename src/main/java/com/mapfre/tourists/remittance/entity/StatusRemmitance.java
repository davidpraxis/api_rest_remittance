/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 25/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.entity;

public class StatusRemmitance {

    private String nom_status;
    private String cod_status;

    public String getNom_status() {
        return nom_status;
    }

    public void setNom_status(String nom_status) {
        this.nom_status = nom_status;
    }

    public String getCod_status() {
        return cod_status;
    }

    public void setCod_status(String cod_status) {
        this.cod_status = cod_status;
    }
}
