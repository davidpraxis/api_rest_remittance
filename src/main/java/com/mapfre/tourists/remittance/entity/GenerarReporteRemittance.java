package com.mapfre.tourists.remittance.entity;

import java.sql.ResultSet;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@NamedStoredProcedureQuery(name = "GenerarReporteRemittance.generarReporteRemittance",
		procedureName = "ea_k_gen_ramo_500_mmx.p_reporte_remittance", parameters = {
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_identificador", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_tip_consulta", type = Integer.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_apply", type = Integer.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_contrato", type = Integer.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_agente", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_aviso_pago", type = Integer.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_poliza", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_from", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_to", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "l_resultado", type = ResultSet.class),
				@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "l_resul_suma", type = ResultSet.class)
		})

@Entity
public class GenerarReporteRemittance {
	
	@Id
	private Long idReporteRemittance;

	public Long getIdReporteRemittance() {
		return idReporteRemittance;
	}

	public void setIdReporteRemittance(Long idReporteRemittance) {
		this.idReporteRemittance = idReporteRemittance;
	}

}
