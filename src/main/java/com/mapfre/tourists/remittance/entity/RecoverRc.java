package com.mapfre.tourists.remittance.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@NamedStoredProcedureQuery(name = "RecoverRc.recoverRc",
	procedureName = "ed_k_gen_ramo_345_mmx.p_recupera_aviso_rc", 
	parameters = {
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_remittance", type = Integer.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "lp_cod_ramo", type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "loc_polizas", type = void.class),
			@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "l_aviso", type = void.class),
			@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "l_agente", type = void.class)

	}
)

@Entity
public class RecoverRc {

	@Id
	private Long idRemittance;

	public Long getIdRemittance() {
		return idRemittance;
	}

	public void setIdRemittance(Long idRemittance) {
		this.idRemittance = idRemittance;
	}

}
