/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedStoredProcedureQuery(name = "Anula.cancelPolicy", procedureName = "p_anula_remmitance", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_cod_ramo", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_aviso_paso", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.OUT, name = "l_err", type = String.class) })

public class Anula implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAnula;

    private String message;

    public Long getIdAnula() {
        return idAnula;
    }

    public void setIdAnula(Long idAnula) {
        this.idAnula = idAnula;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
