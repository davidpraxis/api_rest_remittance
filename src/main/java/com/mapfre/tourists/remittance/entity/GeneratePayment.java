package com.mapfre.tourists.remittance.entity;

import javax.persistence.*;
import java.io.Serializable;
/*
 * Falta validación de tipos de datos
 */
@Entity
@NamedStoredProcedureQuery(name = "GeneratePayment.generatePayment", procedureName = "p_genera_aviso_pago_ramo", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "lp_cod_ramo", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_agente", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_p_neta", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_impuestos", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_comision", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_dp_agt", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "usuario", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "p_banco", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_suplemento", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_iva_trasladado", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_iva_retenido", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_agt_tur", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_der_pol", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_p_total", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_sb_comision", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_imp_agt", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_Poliza", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "p_orden", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "l_isr_retenido", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "l_resultado", type = String.class)

})

public class GeneratePayment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idAviso;

	private String l_resultado;

	public Long getIdAviso() {
		return idAviso;
	}

	public String getL_resultado() {
		return l_resultado;
	}

	public void setIdAviso(Long idAviso) {
		this.idAviso = idAviso;
	}

	public void setL_resultado(String l_resultado) {
		this.l_resultado = l_resultado;
	}

}
