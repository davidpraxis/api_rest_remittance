package com.mapfre.tourists.remittance.entity;

import java.sql.ResultSet;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@NamedStoredProcedureQuery(name = "ConsultaRemittance.consultaRemittance",
        procedureName = "ea_k_gen_ramo_tur_mmx.p_consulta_remittance_ramo", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "lp_cod_ramo", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "tipConsulta", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "pagina", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "registros", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "ordenar", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "tipOrden", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_agente", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_num_contrato", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_fec_from", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_fec_to", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_nom_agt", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_num_aviso_pago", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "l_ape_pat", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "cv_results", type = ResultSet.class)
        

})


@Entity
public class ConsultaRemittance {

    @Id
    private Long idRemittance;
    

	public Long getIdRemittance() {
		return idRemittance;
	}

	public void setIdRemittance(Long idRemittance) {
		this.idRemittance = idRemittance;
	}

    
}
