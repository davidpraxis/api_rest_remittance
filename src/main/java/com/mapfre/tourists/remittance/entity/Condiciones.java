/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 21/01/2022, viernes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.entity;

public class Condiciones {

    private Double num_comision_sal;
    private Double num_sobrecomision_sal;
    private Double der_pol_agt_sal;
    private Double remittance_total_sal;

    public Condiciones(){}

    public Condiciones(Double num_comision_sal, Double num_sobrecomision_sal, Double der_pol_agt_sal, Double remittance_total_sal) {
        this.num_comision_sal = num_comision_sal;
        this.num_sobrecomision_sal = num_sobrecomision_sal;
        this.der_pol_agt_sal = der_pol_agt_sal;
        this.remittance_total_sal = remittance_total_sal;
    }

    public Double getNum_comision_sal() {
        return num_comision_sal;
    }

    public void setNum_comision_sal(Double num_comision_sal) {
        this.num_comision_sal = num_comision_sal;
    }

    public Double getNum_sobrecomision_sal() {
        return num_sobrecomision_sal;
    }

    public void setNum_sobrecomision_sal(Double num_sobrecomision_sal) {
        this.num_sobrecomision_sal = num_sobrecomision_sal;
    }

    public Double getDer_pol_agt_sal() {
        return der_pol_agt_sal;
    }

    public void setDer_pol_agt_sal(Double der_pol_agt_sal) {
        this.der_pol_agt_sal = der_pol_agt_sal;
    }

    public Double getRemittance_total_sal() {
        return remittance_total_sal;
    }

    public void setRemittance_total_sal(Double remittance_total_sal) {
        this.remittance_total_sal = remittance_total_sal;
    }
}
