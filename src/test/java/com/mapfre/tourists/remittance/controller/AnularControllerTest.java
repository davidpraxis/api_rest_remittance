/**
 * Created by : Erick Cruz
 *
 * @Author : ecruz
 * @mailto : e.cruz@praxisglobe.net
 * @created : 18/01/2022, martes
 * @Version 1.0
 **/
package com.mapfre.tourists.remittance.controller;

import com.mapfre.tourists.remittance.model.request.AnulaRequest;
import com.mapfre.tourists.remittance.model.response.AnulaResponse;
import com.mapfre.tourists.remittance.service.AnulaService;
import com.mapfre.tourists.remittance.service.impl.AnulaServiceImpl;
import com.mapfre.tourists.remittance.utils.constants.AnulaConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class AnularControllerTest {

    @Autowired
    AnulaResponse response;

    AnulaService anulaServiceMock = Mockito.mock(AnulaService.class);

    @Autowired
    AnularController anularController = new AnularController();

    @BeforeEach
    void setUp() {
        AnulaRequest request= new AnulaRequest();
        request.setL_cod_ramo(500);
        request.setL_aviso_paso(5);

        anulaServiceMock = new AnulaServiceImpl();
        AnulaResponse response = new AnulaResponse();
        response.setMessage(AnulaConstants.SUCCESS_MESSAGE);

        Mockito.when(anulaServiceMock.cancelPolicy(request)).thenReturn(response);
    }

    @Test
    void cancelPolicyWithValidEntry() {

        AnulaRequest request= new AnulaRequest();
        request.setL_cod_ramo(500);
        request.setL_aviso_paso(5);

        ResponseEntity<AnulaResponse> respuestaServicio;
        if(request != null) {
            respuestaServicio = anularController.anulaPolizasMock(request);
            Assertions.assertEquals(AnulaConstants.SUCCESS_MESSAGE, respuestaServicio.getBody().getMessage());
        }
    }
    @Test
    void cancelPolicyWithInvalidEntry() {
        AnulaRequest request= new AnulaRequest();
        request.setL_cod_ramo(200);
        request.setL_aviso_paso(0);

        ResponseEntity<AnulaResponse> respuestaServicio;
        if(request != null) {
            respuestaServicio = anularController.anulaPolizasMock(request);
            Assertions.assertEquals(AnulaConstants.NOT_SUCCESS_MESSAGE, respuestaServicio.getBody().getMessage());
        }
    }

}
